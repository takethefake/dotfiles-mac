source $HOME/dotfiles/antigen.zsh
ZSH_THEME="lambda-mod"

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

antigen bundle z
antigen bundle fzf
antigen bundle aws 
antigen bundle docker
antigen bundle git
antigen bundle gitfast
antigen bundle kubectl 
antigen bundle npm
antigen bundle osx
antigen bundle vi-mode
antigen bundle yarn 

antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme halfo/lambda-mod-zsh-theme lambda-mod

# Tell Antigen that you're done.
antigen apply

bindkey ^r fzf-history-widget
bindkey ^f fzf-file-widget

source .aliases

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
