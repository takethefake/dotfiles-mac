#!/bin/bash

if ! [ -x "$(command -v brew)" ]; then
    echo "installing brew"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

if ! [ -x "$(command -v nvm)" ]; then
    echo "nvm"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
fi

echo "Installing brew bundles"
brew bundle
if ! [ -f "$HOME/dotfiles/antigen.zsh" ]; then
    echo "installing antigen"
    curl -L git.io/antigen > $HOME/dotfiles/antigen.zsh
fi

env RCRC=$HOME/dotfiles/rcrc rcup
cd $HOME
rcup -d $HOME/dotfiles
ln -s $HOME/dotfiles/vimrc $HOME/.config/nvim/init.vim

if [ $1 = "macos" ]; then
    echo "macos adjustments:"
    sh $HOME/dotfiles/macos.sh
else
    echo "skipping macos update"
fi
nvm install 13
nvm use 13
npm install -g eslint eslint-plugin-react babel-eslint tern
pip3 install --user pynvim

